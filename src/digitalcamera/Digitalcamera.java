/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalcamera;

/**
 *
 * @author HP
 */
abstract class Digitalcamera {
abstract String describeCamera();

}
    
class PointAndShootCamera extends Digitalcamera{
	String model;
        double megapixels;
        double intSize;
        double extSize;
        
	public PointAndShootCamera( String model, double megapixels,double intSize, double extSize) {
		this.model=model;
                this.megapixels=megapixels;
                this.intSize=intSize;
                this.extSize=extSize;
	}
	public String getModel() {
		return model;
	}
        public double getMega() {
		return megapixels;
	}
        public double getInt() {
		return intSize;
	}
        public double getExt() {
		return extSize;
	}
 
	public String describeCamera() {
		return "Model=" +getModel()+ "Megapixels=" +getMega()+ "Internal SIze="+getInt()+"External Size="+getExt();		
	}
}

class PhoneCamera extends Digitalcamera{
	String model1;
        double megapixels1;
        double intSize1;
        double extSize1;
        
	public PhoneCamera( String model1, double megapixels1,double intSize1, double extSize1) {
		this.model1=model1;
                this.megapixels1=megapixels1;
                this.intSize1=intSize1;
                this.extSize1=extSize1;
	}
	public String getModel() {
		return model1;
	}
        public double getMega() {
		return megapixels1;
	}
        public double getInt() {
		return intSize1;
	}
        public double getExt() {
		return extSize1;
	}
 
	public String describeCamera() {
		return "Model=" +getModel()+ "Megapixels=" +getMega()+ "Internal SIze="+getInt()+"External Size="+getExt();		
	}
}

